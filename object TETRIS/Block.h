#pragma once

class Block
{
public:

	Block();
	~Block();

	int ShapeType();
	void InitialPoint(int);

	int s1raw = 0;
	int s1col = 0;
	int s2raw = 1;
	int s2col = 1;
	int s3raw = 2;
	int s3col = 2;
	int s4raw = 3;
	int s4col = 3;

	int rotation = 0;
	int blocktype;
};

