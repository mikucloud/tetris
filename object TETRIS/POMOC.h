#pragma once
#include<iostream>
#include<conio.h>
#include<windows.h>
#include<stdlib.h>
#include"Movements.h"
#include"Graphic.h"


using namespace std;

void Movements::ShapeType()
{
	//blocktype = 2; //define the shapr of block 1-SQ,2-L,3-T,4-I
	blocktype = rand() % 4 + 1;
}

void Movements::InitialPoint()
{

	rotation = 4;
	switch (blocktype)
	{
		//sqaure
	case 1:
		Graphic::s1raw = 0;
		Graphic::s1col = 5;
		Graphic::s2raw = Graphic::s1raw;
		Graphic::s2col = Graphic::s1col + 1;
		Graphic::s3raw = Graphic::s1raw + 1;
		Graphic::s3col = Graphic::s1col;
		Graphic::s4raw = Graphic::s1raw + 1;
		Graphic::s4col = Graphic::s1col + 1;
		break;
		//L
	case 2:
		Graphic::s1raw = 0;
		Graphic::s1col = 5;
		Graphic::s2raw = Graphic::s1raw + 1;
		Graphic::s2col = Graphic::s1col;
		Graphic::s3raw = Graphic::s1raw + 2;
		Graphic::s3col = Graphic::s1col;
		Graphic::s4raw = Graphic::s1raw + 2;
		Graphic::s4col = Graphic::s1col + 1;
		break;
		//pyramid
	case 3:
		Graphic::s1raw = 0;
		Graphic::s1col = 5;
		Graphic::s2raw = Graphic::s1raw + 1;
		Graphic::s2col = Graphic::s1col - 1;
		Graphic::s3raw = Graphic::s1raw + 1;
		Graphic::s3col = Graphic::s1col;
		Graphic::s4raw = Graphic::s1raw + 1;
		Graphic::s4col = Graphic::s1col + 1;
		break;
		//I
	case 4:
		Graphic::s1raw = 0;
		Graphic::s1col = 5;
		Graphic::s2raw = Graphic::s1raw - 1;
		Graphic::s2col = Graphic::s1col;
		Graphic::s3raw = Graphic::s1raw - 2;
		Graphic::s3col = Graphic::s1col;
		Graphic::s4raw = Graphic::s1raw - 3;
		Graphic::s4col = Graphic::s1col;
		break;

	}
}


bool Movements::MoveLeftVer()
{
	bool isitposmoveleft = false;
	int s1colplus = Graphic::s1col - 1;
	int s2colplus = Graphic::s2col - 1;
	int s3colplus = Graphic::s3col - 1;
	int s4colplus = Graphic::s4col - 1;

	if (Graphic::map[Graphic::s1raw][s1colplus] == 'X' ||
		Graphic::map[Graphic::s1raw][s2colplus] == 'X' ||
		Graphic::map[Graphic::s3raw][s3colplus] == 'X' ||
		Graphic::map[Graphic::s4raw][s4colplus] == 'X' ||
		Graphic::s1col <= 0 ||
		Graphic::s2col <= 0 ||
		Graphic::s3col <= 0 ||
		Graphic::s4col <= 0
		)
		isitposmoveleft = true;

	return isitposmoveleft;
}

bool Movements::MoveRightVer()
{
	bool isitposmoveright = false;
	int s1colplus = Graphic::s1col + 1;
	int s2colplus = Graphic::s2col + 1;
	int s3colplus = Graphic::s3col + 1;
	int s4colplus = Graphic::s4col + 1;

	if (Graphic::map[Graphic::s1raw][s1colplus] == 'X' ||
		Graphic::map[Graphic::s1raw][s2colplus] == 'X' ||
		Graphic::map[Graphic::s3raw][s3colplus] == 'X' ||
		Graphic::map[Graphic::s4raw][s4colplus] == 'X' ||
		Graphic::s1col >= 19 ||
		Graphic::s2col >= 19 ||
		Graphic::s3col >= 19 ||
		Graphic::s4col >= 19
		)
		isitposmoveright = true;

	return isitposmoveright;
}

void Movements::Move()
{
	switch (ReturnArrowKey())
	{
	case 'u':
		Graphic::s1raw--;
		Graphic::s2raw--;
		Graphic::s3raw--;
		Graphic::s4raw--;
		break;
	case 'd':
		Graphic::s1raw++;
		Graphic::s2raw++;
		Graphic::s3raw++;
		Graphic::s4raw++;
		break;
	case 'r':
		if (MoveRightVer())
		{

		}
		else
		{
			Graphic::s1col++;
			Graphic::s2col++;
			Graphic::s3col++;
			Graphic::s4col++;
		}

		break;
	case 'l':
		if (MoveLeftVer())
		{

		}
		else
		{
			Graphic::s1col--;
			Graphic::s2col--;
			Graphic::s3col--;
			Graphic::s4col--;
		}
		break;
	case'o':
	{
		ChooseRotate();
		rotation++;
	}
	break;
	case'e':
	{}
	break;

	}
}

void Movements::ChooseRotate()
{
	switch (blocktype)
	{
		//case 1:
		//RotateSQ();
		//break;
	case 2:
		RotateL();
		break;
	case 3:
		RotateT();
		break;
	case 4:
		RotateI();
		break;
	}
}

void Movements::CheckPossibleRotation(int s1r, int s1c, int s2r, int s2c, int s3r, int s3c, int s4r, int s4c)
{

	if (
		!(Graphic::map[s1r][s1c] == 'X' ||
			Graphic::map[s2r][s2c] == 'X' ||
			Graphic::map[s3r][s3c] == 'X' ||
			Graphic::map[s4r][s4c] == 'X'
			))
	{
		Graphic::s1raw = s1r;
		Graphic::s1col = s1c;
		Graphic::s2raw = s2r;
		Graphic::s2col = s2c;
		Graphic::s3raw = s3r;
		Graphic::s3col = s3c;
		Graphic::s4raw = s4r;
		Graphic::s4col = s4c;
	}
}

void Movements::RotateSQ()
{
	//no rotation
}

void Movements::RotateL()
{
	int s1rTemp;
	int s1cTemp;
	int s2rTemp;
	int s2cTemp;
	int s3rTemp;
	int s3cTemp;
	int s4rTemp;
	int s4cTemp;

	switch ((rotation % 4) + 1)
	{
	case 1:
	{
		s1rTemp = Graphic::s1raw + 1;
		s1cTemp = Graphic::s1col + 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::Graphic::s3raw - 1;
		s3cTemp = Graphic::s3col - 1;
		s4rTemp = Graphic::s4raw;
		s4cTemp = Graphic::s4col - 2;
	}
	break;

	case 2:
	{
		s1rTemp = Graphic::s1raw + 1;
		s1cTemp = Graphic::s1col - 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::s3raw - 1;
		s3cTemp = Graphic::s3col + 1;
		s4rTemp = Graphic::s4raw - 2;
		s4cTemp = Graphic::s4col;
	}
	break;

	case 3:
	{
		s1rTemp = Graphic::s1raw - 1;
		s1cTemp = Graphic::s1col - 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::s3raw + 1;
		s3cTemp = Graphic::s3col + 1;
		s4rTemp = Graphic::s4raw;
		s4cTemp = Graphic::s4col + 2;
	}
	break;

	case 4:
	{
		s1rTemp = Graphic::s1raw - 1;
		s1cTemp = Graphic::s1col + 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::s3raw + 1;
		s3cTemp = Graphic::s3col - 1;
		s4rTemp = Graphic::s4raw + 2;
		s4cTemp = Graphic::s4col;
	}
	}

	CheckPossibleRotation(s1rTemp, s1cTemp, s2rTemp, s2cTemp, s3rTemp, s3cTemp, s4rTemp, s4cTemp);
}

void Movements::RotateT()
{
	int s1rTemp;
	int s1cTemp;
	int s2rTemp;
	int s2cTemp;
	int s3rTemp;
	int s3cTemp;
	int s4rTemp;
	int s4cTemp;

	switch ((rotation % 4) + 1)
	{
	case 1:
	{
		s1rTemp = Graphic::s1raw + 1;
		s1cTemp = Graphic::s1col + 1;
		s2rTemp = Graphic::s2raw - 1;
		s2cTemp = Graphic::s2col + 1;
		s3rTemp = Graphic::s3raw;
		s3cTemp = Graphic::s3col;
		s4rTemp = Graphic::s4raw + 1;
		s4cTemp = Graphic::s4col - 1;
	}
	break;

	case 2:
	{
		s1rTemp = Graphic::s1raw + 1;
		s1cTemp = Graphic::s1col - 1;
		s2rTemp = Graphic::s2raw + 1;
		s2cTemp = Graphic::s2col + 1;
		s3rTemp = Graphic::s3raw;
		s3cTemp = Graphic::s3col;
		s4rTemp = Graphic::s4raw - 1;
		s4cTemp = Graphic::s4col - 1;
	}
	break;

	case 3:
	{
		s1rTemp = Graphic::s1raw - 1;
		s1cTemp = Graphic::s1col - 1;
		s2rTemp = Graphic::s2raw + 1;
		s2cTemp = Graphic::s2col - 1;
		s3rTemp = Graphic::s3raw;
		s3cTemp = Graphic::s3col;
		s4rTemp = Graphic::s4raw - 1;
		s4cTemp = Graphic::s4col + 1;
	}
	break;

	case 4:
	{
		s1rTemp = Graphic::s1raw - 1;
		s1cTemp = Graphic::s1col + 1;
		s2rTemp = Graphic::s2raw - 1;
		s2cTemp = Graphic::s2col - 1;
		s3rTemp = Graphic::s3raw;
		s3cTemp = Graphic::s3col;
		s4rTemp = Graphic::s4raw + 1;
		s4cTemp = Graphic::s4col + 1;
	}
	}

	CheckPossibleRotation(s1rTemp, s1cTemp, s2rTemp, s2cTemp, s3rTemp, s3cTemp, s4rTemp, s4cTemp);

}

void Movements::RotateI()
{

	int s1rTemp;
	int s1cTemp;
	int s2rTemp;
	int s2cTemp;
	int s3rTemp;
	int s3cTemp;
	int s4rTemp;
	int s4cTemp;

	switch ((rotation % 4) + 1)
	{
	case 1:
	{
		s1rTemp = Graphic::s1raw - 1;
		s1cTemp = Graphic::s1col - 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::s3raw + 1;
		s3cTemp = Graphic::s3col + 1;
		s4rTemp = Graphic::s4raw + 2;
		s4cTemp = Graphic::s4col + 2;
	}
	break;
	case 2:
	{
		s1rTemp = Graphic::s1raw - 1;
		s1cTemp = Graphic::s1col + 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::s3raw + 1;
		s3cTemp = Graphic::s3col - 1;
		s4rTemp = Graphic::s4raw + 2;
		s4cTemp = Graphic::s4col - 2;
	}
	break;
	case 3:
	{
		s1rTemp = Graphic::s1raw + 1;
		s1cTemp = Graphic::s1col + 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::s3raw - 1;
		s3cTemp = Graphic::s3col - 1;
		s4rTemp = Graphic::s4raw - 2;
		s4cTemp = Graphic::s4col - 2;
	}
	break;

	case 4:
	{
		s1rTemp = Graphic::s1raw + 1;
		s1cTemp = Graphic::s1col - 1;
		s2rTemp = Graphic::s2raw;
		s2cTemp = Graphic::s2col;
		s3rTemp = Graphic::s3raw - 1;
		s3cTemp = Graphic::s3col + 1;
		s4rTemp = Graphic::s4raw - 2;
		s4cTemp = Graphic::s4col + 2;
	}
	break;
	}
	CheckPossibleRotation(s1rTemp, s1cTemp, s2rTemp, s2cTemp, s3rTemp, s3cTemp, s4rTemp, s4cTemp);
}
char Movements::ReturnArrowKey()
{
	int ReadArrowKey0;
	int ReadArrowKey1;

	char Key = 'e';
	//char U; //Up arrow
	//char R; //Right arrow
	//char D; //Down arrow
	//char L; //Left arrow

	ReadArrowKey0 = _getch();
	if (ReadArrowKey0 == 224)
	{
		//cout << co;
		ReadArrowKey1 = _getch();
		switch (ReadArrowKey1)
		{
		case 72:
			Key = 'u';
			break;
		case 77:
			Key = 'r';
			break;
		case 75:
			Key = 'l';
			break;
		case 80:
			Key = 'd';
			break;
		}

	}
	if (ReadArrowKey0 == 32)
	{
		Key = 'o';

	}
	else
	{
	}
	return Key;
}