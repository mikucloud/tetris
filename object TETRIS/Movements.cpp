#include<iostream>
#include<conio.h>
#include<windows.h>
#include<stdlib.h>
#include"Movements.h"
#include"Graphic.h"
#include"Block.h"


using namespace std;

//Set the auxiliary coordinate movexxx on the actual values (taken from block object)
void Movements::MoveCoordGet(const Block& B)
{
	 moves1raw = B.s1raw;
	 moves1col = B.s1col;
	 moves2raw = B.s2raw;
	 moves2col = B.s2col;
	 moves3raw = B.s3raw;
	 moves3col = B.s3col;
	 moves4raw = B.s4raw;
	 moves4col = B.s4col;

	 moveShapeType = B.blocktype;
	 moveRotation = B.rotation;
  }

//Look up if performed moves are legal - if yes this fce set the coordinates in block object on the changed value (auxiliary coord.)
void Movements::MoveCoordSet(Block& B)
{
	if(!moveVer)
	{
 		B.s1raw = moves1raw;
		B.s1col = moves1col;
		B.s2raw = moves2raw;
		B.s2col = moves2col;
		B.s3raw = moves3raw;
		B.s3col = moves3col;
		B.s4raw = moves4raw;
		B.s4col = moves4col;
	}

	 B.rotation= moveRotation;
}



//Verify if the auxiliary coord changed bz move fce are legal.
void Movements::MoveVer(const Graphic& G)
{
	moveVer = false;

	if (G.map[moves1raw][moves1col] == 'X' ||
		G.map[moves2raw][moves2col] == 'X' ||
		G.map[moves3raw][moves3col] == 'X' ||
		G.map[moves4raw][moves4col] == 'X' ||
		moves1col > (G.horizontalDim - 2) ||
		moves2col > (G.horizontalDim - 2) ||
		moves3col > (G.horizontalDim - 2) ||
		moves4col > (G.horizontalDim - 2) ||
		moves1col < 1 ||
		moves2col < 1 ||
		moves3col < 1 ||
		moves4col < 1
		)
		moveVer = true;

	
}

//receive the return value from ReturnArrowKey fce and change the auxiliarz coord bz corresponding matter
void Movements::Move()
{
	switch (ReturnArrowKey())
	{
	case 'u':
		moves1raw--;
		moves2raw--;
		moves3raw--;
		moves4raw--;
		break;
	case 'd':
		moves1raw++;
		moves2raw++;
		moves3raw++;
		moves4raw++;
		break;
	case 'r':
			moves1col++;
			moves2col++;
			moves3col++;
			moves4col++;
		break;
	case 'l':
			moves1col--;
			moves2col--;
			moves3col--;
			moves4col--;
		break;
	case'o': 
	{
		ChooseRotate();
		moveRotation++;
	}
	break;
	case'e':
	{}
	break;

	}
}

//based on auxiliary variable moveShapeType decide which rotation function will be called
void Movements::ChooseRotate()
{
	switch (moveShapeType)
	{
		//case 1:
		//RotateSQ();
		//break;
	case 2:
		RotateL();
		break;
	case 3:
		RotateT();
		break;
	case 4:
		RotateI();
		break;
	}
}

//Rotation function modifz the auxiliary coord in specific way- based on shape type.

void Movements::RotateSQ()
{
	//no rotation
}

void Movements::RotateL()
{
	switch ((moveRotation % 4) + 1)
	{
	case 1:
	{
		moves1raw = moves1raw + 1;
		moves1col = moves1col + 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw - 1;
		moves3col = moves3col - 1;
		moves4raw = moves4raw;
		moves4col = moves4col - 2;
	}
	break;

	case 2:
	{
		moves1raw = moves1raw + 1;
		moves1col = moves1col - 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw - 1;
		moves3col = moves3col + 1;
		moves4raw = moves4raw - 2;
		moves4col = moves4col;
	}
	break;

	case 3:
	{
		moves1raw = moves1raw - 1;
		moves1col = moves1col - 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw + 1;
		moves3col = moves3col + 1;
		moves4raw = moves4raw;
		moves4col = moves4col + 2;
	}
	break;

	case 4:
	{
		moves1raw = moves1raw - 1;
		moves1col = moves1col + 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw + 1;
		moves3col = moves3col - 1;
		moves4raw = moves4raw + 2;
		moves4col = moves4col;
	}
	}

	
}

void Movements::RotateT()
{
	switch ((moveRotation % 4) + 1)
	{
	case 1:
	{
		moves1raw = moves1raw + 1;
		moves1col = moves1col + 1;
		moves2raw = moves2raw - 1;
		moves2col = moves2col + 1;
		moves3raw = moves3raw;
		moves3col = moves3col;
		moves4raw = moves4raw + 1;
		moves4col = moves4col - 1;
	}
	break;

	case 2:
	{
		moves1raw = moves1raw + 1;
		moves1col = moves1col - 1;
		moves2raw = moves2raw + 1;
		moves2col = moves2col + 1;
		moves3raw = moves3raw;
		moves3col = moves3col;
		moves4raw = moves4raw - 1;
		moves4col = moves4col - 1;
	}
	break;

	case 3:
	{
		moves1raw = moves1raw - 1;
		moves1col = moves1col - 1;
		moves2raw = moves2raw + 1;
		moves2col = moves2col - 1;
		moves3raw = moves3raw;
		moves3col = moves3col;
		moves4raw = moves4raw - 1;
		moves4col = moves4col + 1;
	}
	break;

	case 4:
	{
		moves1raw = moves1raw - 1;
		moves1col = moves1col + 1;
		moves2raw = moves2raw - 1;
		moves2col = moves2col - 1;
		moves3raw = moves3raw;
		moves3col = moves3col;
		moves4raw = moves4raw + 1;
		moves4col = moves4col + 1;
	}
	}

	

}

void Movements::RotateI()
{

	
	switch ((moveRotation % 4) + 1)
	{
	case 1:
	{
		moves1raw = moves1raw - 1;
		moves1col = moves1col - 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw + 1;
		moves3col = moves3col + 1;
		moves4raw = moves4raw + 2;
		moves4col = moves4col + 2;
	}
	break;
	case 2:
	{
		moves1raw = moves1raw - 1;
		moves1col = moves1col + 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw + 1;
		moves3col = moves3col - 1;
		moves4raw = moves4raw + 2;
		moves4col = moves4col - 2;
	}
	break;
	case 3:
	{
		moves1raw = moves1raw + 1;
		moves1col = moves1col + 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw - 1;
		moves3col = moves3col - 1;
		moves4raw = moves4raw - 2;
		moves4col = moves4col - 2;
	}
	break;

	case 4:
	{
		moves1raw = moves1raw + 1;
		moves1col = moves1col - 1;
		moves2raw = moves2raw;
		moves2col = moves2col;
		moves3raw = moves3raw - 1;
		moves3col = moves3col + 1;
		moves4raw = moves4raw - 2;
		moves4col = moves4col + 2;
	}
	break;
	}
	
}

//detect the key press and return the corresponding value
char Movements::ReturnArrowKey()
{
	int ReadArrowKey0;
	int ReadArrowKey1;

	char Key = 'e';
	//char U; //Up arrow
	//char R; //Right arrow
	//char D; //Down arrow
	//char L; //Left arrow

	ReadArrowKey0 = _getch();
	if (ReadArrowKey0 == 224)
	{
		//cout << co;
		ReadArrowKey1 = _getch();
		switch (ReadArrowKey1)
		{
		case 72:
			Key = 'u';
			break;
		case 77:
			Key = 'r';
			break;
		case 75:
			Key = 'l';
			break;
		case 80:
			Key = 'd';
			break;
		}

	}
	if (ReadArrowKey0 == 32)
	{
		Key = 'o';

	}
	else
	{
	}
	return Key;
}