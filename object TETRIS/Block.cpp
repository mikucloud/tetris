#include<iostream>
#include<conio.h>
#include<windows.h>
#include<stdlib.h>
#include"Movements.h"
#include"Graphic.h"
#include"Block.h"

using namespace std;

Block::Block()
{
	InitialPoint(ShapeType());
}

Block::~Block()
{}

//define the shape of block 1-SQ,2-L,3-T,4-I
int Block::ShapeType()
{
	//blocktype = 2; 
	blocktype = rand() % 4 + 1;
	return blocktype;
	
}

//create current block in starting position. set the intitial coordinates of every block part.
void Block::InitialPoint(int shape)
{

	
	switch (shape)
	{
		//sqaure
	case 1:
		s1raw = 1;
		s1col = 5;
		s2raw = s1raw;
		s2col = s1col + 1;
		s3raw = s1raw + 1;
		s3col = s1col;
		s4raw = s1raw + 1;
		s4col = s1col + 1;
		break;
		//L
	case 2:
		s1raw = 1;
		s1col = 5;
		s2raw = s1raw + 1;
		s2col = s1col;
		s3raw = s1raw + 2;
		s3col = s1col;
		s4raw = s1raw + 2;
		s4col = s1col + 1;
		break;
		//pyramid
	case 3:
		s1raw = 1;
		s1col = 5;
		s2raw = s1raw + 1;
		s2col = s1col - 1;
		s3raw = s1raw + 1;
		s3col = s1col;
		s4raw = s1raw + 1;
		s4col = s1col + 1;
		break;
		//I
	case 4:
		s1raw = 3;
		s1col = 5;
		s2raw = s1raw - 1;
		s2col = s1col;
		s3raw = s1raw - 2;
		s3col = s1col;
		s4raw = s1raw - 3;
		s4col = s1col;
		break;

	}
}
