#pragma once
#include"Graphic.h"
#include"Block.h"

class Movements:public Graphic
{
public:
	void MoveCoordGet(const Block& B);
	void MoveCoordSet(Block& B);
	void Move();


	
	void MoveVer(const Graphic& G);


	private:
	char ReturnArrowKey();
	
	void ChooseRotate();
	void RotateSQ();
	void RotateL();
	void RotateT();
	void RotateI();
	void CheckPossibleRotation(int s1r, int s1c, int s2r, int s2c, int s3r, int s3c, int s4r, int s4c);


	int moves1raw = 0;
	int moves1col = 0;
	int moves2raw = 0;
	int moves2col = 0;
	int moves3raw = 0;
	int moves3col = 0;
	int moves4raw = 0;
	int moves4col = 0;

	int moveShapeType = 0;
	int moveRotation = 0;

	bool moveVer=false;
	//bool moveLeft = false;
};
