#pragma once
#include"Block.h"
#include"Movements.h"

class Graphic
	{
	public:
		Graphic();
		//~Graphic();

		void CreateMap();
		void FallDown(Block& B);
		bool PermanentPoint(const Block& B);
		void PrintMap(const Block&B);
		void CreatePoint(const Block& B);
		void DeleteFullRaw();
		bool EndGame(const Block& B);
		

		//private:
		static const int horizontalDim = 20;
		static const int verticalDim = 15;
		char map[verticalDim][horizontalDim];
		int score = 0;
};
