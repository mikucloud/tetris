#include<iostream>
#include<conio.h>
#include<windows.h>
#include<stdlib.h>
#include"Movements.h"
#include"Graphic.h"
#include"Block.h"


using namespace std;

Graphic::Graphic()
{
	for (int i = 0; i < (verticalDim); i++)
		for (int j = 0; j < (horizontalDim); j++)
		{
			
				map[i][j] = '0';
		}
}




//Fill the map array with dimensions inputed in Graphic.h. All positions are filled with dots first  and secondary by space on the other positions.
//Both cycles skip positions where the X char is.
void Graphic::CreateMap()
{
	for (int i = 0; i < (verticalDim ); i++)
		for (int j = 0; j < (horizontalDim); j++)
		{
			if (!(map[i][j] == 'X'))
				map[i][j] = '.';
		}

	for (int i = 1; i < (verticalDim-1); i++)
		for (int j = 1; j < (horizontalDim-1); j++)
		{
			if (!(map[i][j] == 'X'))
				map[i][j] = ' ';
		}
}



// print the actual map array.
void Graphic::PrintMap(const Block&B)
{
	for (int i = 0; i < (verticalDim); i++)
	{
		for (int j = 0; j < (horizontalDim); j++)
		{
			cout << map[i][j];
		}
		cout << endl;
	}
	cout << "score:" << score<<"\n";
	//cout << "shapetype:" << B.blocktype << "\n";
	//cout << "rotation:" << B.rotation << "\n";
	//cout << map[B.s1raw][B.s1col-1];
	//cout << map[B.s2raw][B.s2col-1];
	//cout << map[B.s3raw][B.s3col-1];
	//cout << map[B.s4raw][B.s4col-1]<<"\n";
	//cout << map[M.moves1raw][M.moves1col - 1];
	//cout << map[M.moves2raw][M.moves2col - 1];
	//cout << map[M.moves3raw][M.moves3col - 1];
	//cout << map[M.moves4raw][M.moves4col - 1];



}

//When no arrow or space button is not pressed in current time interval this fce is called and move the block down.
void Graphic::FallDown(Block& B)
{
	B.s1raw++;
	B.s2raw++;
	B.s3raw++;
	B.s4raw++;
}

//Recognize that the block reach the bottom or any other already settled blocks (char X in map array). 
//If Yes than revrite the block into X char  and return false to brake the main loop.
bool Graphic::PermanentPoint(const Block& B)
{

	bool newpoint = true;
	int s1rawplus = B.s1raw + 1;
	int s2rawplus = B.s2raw + 1;
	int s3rawplus = B.s3raw + 1;
	int s4rawplus = B.s4raw + 1;

	if (B.s1raw == (verticalDim-2)|| map[s1rawplus][B.s1col] == 'X'
		||
		B.s2raw == (verticalDim - 2) || map[s2rawplus][B.s2col] == 'X'
		||
		B.s3raw == (verticalDim - 2) || map[s3rawplus][B.s3col] == 'X'
		||
		B.s4raw == (verticalDim - 2) || map[s4rawplus][B.s4col] == 'X'
		)
	{
		map[B.s1raw][B.s1col] = 'X';
		map[B.s2raw][B.s2col] = 'X';
		map[B.s3raw][B.s3col] = 'X';
		map[B.s4raw][B.s4col] = 'X';
		newpoint = false;
	}
	return newpoint;
}

//If fce PermanentPoint return false this function verfifz if fanz raw is full of X char
//- if yes than fill the raw with spaces and all X chars in higher raws move down and ++ the score
void Graphic::DeleteFullRaw()
{
	int NumberOfXInRaw = 0;
	//int fullRaw;
	for (int i = 1; i < (verticalDim-1); i++)
	{
		NumberOfXInRaw = 0;
		for (int j = 1; j < (horizontalDim-1); j++)
		{
			if ((map[i][j] == 'X'))
			{
				NumberOfXInRaw++;
				if (NumberOfXInRaw == (horizontalDim-2))
				{
					for (int r = 1; r < (horizontalDim-1); r++)
						map[i][r] = ' ';
					for (int k = i; k > 0; k--)
					{
						for (int l = 1; l < (horizontalDim-1); l++)
						{
							if (map[k][l] == 'X')
							{
								map[k][l] = ' ';
								map[k + 1][l] = 'X';

							}

						}
					}
					score++;
				}
			}
			else
				break;
		}
	}
}

//return true if the any postion in the first game raw is filled by X char
bool Graphic::EndGame(const Block& B)
{
	bool endgame = false;
	if(
		map[B.s1raw][B.s1col] == 'X'
		||
		map[B.s2raw][B.s2col] == 'X'
		||
		map[B.s3raw][B.s3col] == 'X'
		||
		map[B.s4raw][B.s4col] == 'X'
		)
	//for (int j = 1; j < horizontalDim - 1; j++)
	//{
	//	if (map[1][j] == 'X')
	//	{
	endgame = true;
	//	break;
	//	}
	//}
	return endgame;

}

//Write the actual coordinates of block into the map array
void Graphic::CreatePoint(const Block& B)
{
	map[B.s1raw][B.s1col] = 'A';
	map[B.s2raw][B.s2col] = 'A';
	map[B.s3raw][B.s3col] = 'A';
	map[B.s4raw][B.s4col] = 'A';
}
